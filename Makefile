default: build

build:
	docker build \
		-t sneak/steemd \
		$(BUILD_ARGS) \
		.

run: kill
	envdir ~/.paths/steem-secrets \
		docker-compose up -d

kill:
	docker-compose kill
	docker-compose rm -f
