FROM phusion/baseimage:0.9.19

ARG STEEMD_BLOCKCHAIN=https://cdn.datavibe.net/f/eeqj/20160906.steemd-blockchain.tbz2

# steem:master as of 2016-08-18
ARG STEEMD_REPO=https://github.com/steemit/steem.git
ARG STEEMD_REV=master

# secp256k1:master as of 2016-08-18
ARG SECP256K1_REPO=https://github.com/bitcoin/secp256k1
ARG SECP256K1_REV=master

ARG UBUNTU_MIRROR=mirror://mirrors.ubuntu.com/mirrors.txt

RUN sed -i \
    -e s#http://archive.ubuntu.com/ubuntu/#${UBUNTU_MIRROR}#g \
    -e s#http://security.ubuntu.com/ubuntu/#${UBUNTU_MIRROR}#g \
        /etc/apt/sources.list ; \
        grep -v deb-src /etc/apt/sources.list > \
            /etc/apt/sources.list.new && \
        mv /etc/apt/sources.list.new /etc/apt/sources.list && \
        cat /etc/apt/sources.list

RUN \
    apt-get update && \
    apt-get install -y \
        autoconf \
        automake \
        autotools-dev \
        bsdmainutils \
        build-essential \
        cmake \
        doxygen \
        git \
        libboost-all-dev \
        libreadline-dev \
        libssl-dev \
        libtool \
        ncurses-dev \
        pbzip2 \
        python3 \
        python3-dev \
    && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

RUN \
    git clone \
        $SECP256K1_REPO \
        /usr/local/src/secp256k1 && \
    cd /usr/local/src/secp256k1 && \
    git checkout $SECP256K1_REV && \
    ./autogen.sh && \
    ./configure && \
    make -j$(nproc) && \
    ./tests && \
    make install && \
    cd / && \
    rm -rfv /usr/local/src/secp256k1

RUN \
    git clone \
        $STEEMD_REPO \
        /usr/local/src/steem && \
    cd /usr/local/src/steem && \
    git checkout $STEEMD_REV && \
    git submodule update --init --recursive && \
    rsync -a \
        /usr/local/src/steem/ \
        /usr/local/src/steemtest/

RUN \
    cd /usr/local/src/steemtest && \
    cmake \
        -DCMAKE_BUILD_TYPE=Debug \
        -DBUILD_STEEM_TESTNET=On \
        -DLOW_MEMORY_NODE=ON \
        . \
    && \
    make -j$(nproc) chain_test && \
    ./tests/chain_test && \
    rm -rf /usr/local/src/steemtest

RUN \
    cd /usr/local/src/steem && \
    cmake \
        -DCMAKE_BUILD_TYPE=Release \
        -DLOW_MEMORY_NODE=ON \
        . \
    && \
    make -j$(nproc) && \
    make install && \
    rm -rf /usr/local/src/steem

RUN \
    apt-get remove -y \
        automake \
        autotools-dev \
        bsdmainutils \
        build-essential \
        cmake \
        doxygen \
        dpkg-dev \
        git \
        libboost-all-dev \
        libc6-dev \
        libexpat1-dev \
        libgcc-5-dev \
        libhwloc-dev \
        libibverbs-dev \
        libicu-dev \
        libltdl-dev \
        libncurses5-dev \
        libnuma-dev \
        libopenmpi-dev \
        libpython-dev \
        libpython2.7-dev \
        libreadline-dev \
        libreadline6-dev \
        libssl-dev \
        libstdc++-5-dev \
        libtinfo-dev \
        libtool \
        linux-libc-dev \
        m4 \
        make \
        manpages \
        manpages-dev \
        mpi-default-dev \
        python-dev \
        python2.7-dev \
        python3-dev \
    && \
    apt-get autoremove -y && \
    rm -rf \
        /var/lib/apt/lists/* \
        /tmp/* \
        /var/tmp/* \
        /var/cache/* \
        /usr/include \
        /usr/local/include

# add blockchain cache to image
ADD $STEEMD_BLOCKCHAIN /var/cache/steemd/blocks.tbz2

RUN chmod a+rx /var/cache/steemd /var/cache/steemd/*

ENV HOME /var/lib/steemd
RUN useradd -s /bin/bash -m -d /var/lib/steemd steemd
RUN chown steemd:steemd -R /var/lib/steemd

VOLUME ["/var/lib/steemd"]

# rpc service:
EXPOSE 8090
# p2p service:
EXPOSE 2001

RUN mkdir -p /etc/service/steemd
ADD steemd.run /etc/service/steemd/run
RUN chmod +x /etc/service/steemd/run

ADD config.ini /etc/steemd/config.ini
